Simple and libre CNC lathe simulator written in Tcl/Tk. *Early in development.*

### Download

- **Windows** — [Release 0.1.1](https://codeberg.org/attachments/bd289673-9177-43ca-817b-b459b30dc4c5)
- **Linux** — Install `tk` from your distribution's repository, run `./simin.tcl`.

### Commands

Simin reads text input line by line, therefore each new block is a new point.

- TRANS Z... — Translate the zero point on the Z axis.
- X... — X-coordinate used in linear movement (y in code). 
- Z... — Z-coordinate used in linear movement (x in code).

Anything else may be part of the input but will be ignored.

### License

Simin is licensed under the GNU GPLv3 license, or later at your choice.

Attribution:

- Windows binary is compressed with UPX.
- Temporary icon by Wishforge Games. CC-BY.
