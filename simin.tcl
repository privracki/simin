#!/usr/bin/wish

# Use Tk
package require Tk

# Set window title
wm title . "Simin"
wm geometry . 800x600+100+100
wm minsize . 320 240

# Initialize widgets
ttk::frame       .main -padding "3 0 3 3"
ttk::button      .main.run -text "Start" -command simulate
ttk::label       .main.sizelbl -text "Size:"
ttk::spinbox     .main.size -from 1.0 -to 10.0 -textvariable sizemod -width 2
ttk::panedwindow .main.pane -orient horizontal
tk::text         .main.pane.code -width 40
tk::canvas       .main.pane.sim -background #fff

# Put everything on screen
grid .main           -column 0 -row 0 -sticky nwes
grid .main.run       -column 0 -row 0 -sticky w -pady 3
grid .main.sizelbl   -column 2 -row 0 -sticky e -pady 3
grid .main.size      -column 3 -row 0 -sticky e -padx 3 -pady 3
grid .main.pane      -column 0 -row 1 -columnspan 4 -sticky nwes
grid .main.pane.code -column 0 -row 1 -sticky nwes
grid .main.pane.sim  -column 3 -row 1 -sticky nwes

# Add text and canvas to paned window
.main.pane add .main.pane.code
.main.pane add .main.pane.sim

# Make everything resizable
grid columnconfigure . 0 -weight 1
grid rowconfigure    . 0 -weight 1
grid columnconfigure .main 1 -weight 1
grid rowconfigure    .main 1 -weight 1

# Focus on text window
focus .main.pane.code

#
# Procedures
#

# Coordinate multiplier
set ::sizemod 2

# Simulation executed by the "Start" button
proc simulate {} {
	# Initialize old coordinate variables
	set ::x {}
	set ::y {}

	# Initialise new coordinate variables
	set inx 0
	set iny 0
	set transz 0

	# Clear previous run
	.main.pane.sim delete all

	# Split text widget input into a list on each newline
	set input [split [.main.pane.code get 1.0 end] "\n"]

	# Read each line
	foreach line $input {
		# Parse TRANS Z command
		if {[regexp -nocase {trans z(\d+)} $line all transz] == 1} {
			set transz $transz
			# Skip iteration to avoid Z being read on its own
			continue
		}

		# Parse X command
		if {[regexp -nocase {x(-*\d+)} $line all newy] == 1} {
			set newy [expr (($newy * -1 * $::sizemod) + 300)]
			if {$::y eq {}} {
				set ::y $newy
			}
			set iny $newy
		}

		# Parse Z command
		if {[regexp -nocase {z(-*\d+)} $line all newx] == 1} {
			set newx [expr ($newx + $transz) * $::sizemod]
			if {$::x eq {}} {
				set ::x $newx
			}
			set inx $newx
		}

		drawLine $inx $iny
	}
}

proc drawLine {newx newy} {
	.main.pane.sim create line $::x $::y $newx $newy
	puts "> DrawLine $::x,$::y to $newx,$newy"
	set ::x $newx
	set ::y $newy
}
